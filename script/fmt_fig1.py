"""
Format weather measurements from fig2
"""
from pathlib import Path

import pandas as pd
from graphextract.svg_extractor import extract_data

from harley1992 import pth_clean

records = []

for ca, suffix in [(35, 'a'), (65, 'b')]:
    data = extract_data(f"../raw/fig1{suffix}.svg", x_formatter=int, y_formatter=int)

    for o2, pts in data.items():
        for ci, an in sorted(pts):
            records.append(dict(
                ca=ca,
                o2=o2,
                ci=ci,
                an=an
            ))

df = pd.DataFrame(records).set_index(['ca', 'o2']).sort_index()

# write resulting dataframe in a csv file
df = df[sorted(df.columns)]

with open(pth_clean / "fig1.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("#\n")
    fhw.write("# ca: [Pa CO2] partial pressure of CO2 during growth\n")
    fhw.write("# o2: [kPa] partial pressure of O2 in atmosphere\n")
    fhw.write("# an: [µmol CO2.m-2.s-1] Net photosynthesis\n")
    fhw.write("# ci: [Pa] Intercellular CO2\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
