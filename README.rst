========================
harley1992
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/harley1992/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/harley1992/1.1.0/

.. image:: https://b326.gitlab.io/harley1992/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/harley1992

.. image:: https://b326.gitlab.io/harley1992/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/harley1992/

.. image:: https://badge.fury.io/py/harley1992.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/harley1992

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/harley1992/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/harley1992/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/harley1992/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/harley1992/commits/main
.. #}

Data and formalisms from Harley 1992 publication

