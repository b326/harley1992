"""
Figure 9
========

Plot formalisms using parameter values from table1
"""
# %%
# .. warning::
#
#    Error in legend of figure 9 in the article.
#
#    Legend of figure is at best misleading. Apparently used Ci while implying Ca.
#

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from harley1992 import pth_clean, raw

# read meas
meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#", index_col=['mod_growth'])

# params
tab1 = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['name', 'ca'])['value']
t_leaf = 29  # [°C] from fig8 legend
tk = t_leaf + 273.15
ppfd = 1000  # [µmol.m-2.s-1] from fig8 legend
rh = 50  # [%] from fig8 legend

o2 = 20e3  # [Pa] no mention anywhere

# recompute curves
dfs = []

for mod_growth in (35, 65):
    alpha = tab1.at[('alpha', mod_growth)]
    rd = tab1.at[('rd', mod_growth)]
    g0 = tab1.at[('g0', mod_growth)]
    gt = tab1.at[('gt', mod_growth)]

    tau = raw.eq8(tk, tab1.at[('tau_c', mod_growth)], tab1.at[('tau_deltaha', mod_growth)])
    kc = raw.eq8(tk, tab1.at[('kc_c', mod_growth)], tab1.at[('kc_deltaha', mod_growth)])
    ko = raw.eq8(tk, tab1.at[('ko_c', mod_growth)], tab1.at[('ko_deltaha', mod_growth)])

    vc_max = raw.eq9(tk,
                     tab1.at[('vc_max_c', mod_growth)],
                     tab1.at[('vc_max_deltaha', mod_growth)],
                     tab1.at[('vc_max_deltahd', mod_growth)],
                     tab1.at[('vc_max_deltas', mod_growth)])
    j_max = raw.eq9(tk,
                    tab1.at[('j_max_c', mod_growth)],
                    tab1.at[('j_max_deltaha', mod_growth)],
                    tab1.at[('j_max_deltahd', mod_growth)],
                    tab1.at[('j_max_deltas', mod_growth)])
    tpu = raw.eq9(tk,
                  tab1.at[('tpu_c', mod_growth)],
                  tab1.at[('tpu_deltaha', mod_growth)],
                  tab1.at[('tpu_deltahd', mod_growth)],
                  tab1.at[('tpu_deltas', mod_growth)])

    records = []
    for ca in np.linspace(8, 80, 100):
        an, ci, gs = raw.algo(ppfd, rh, ca, o2, g0, gt, tau, rd, vc_max, kc, ko, alpha, j_max, tpu)
        records.append(dict(
            ca=ca,
            ci=ci,
            an=an,
            gs=gs
        ))

    dfs.append([mod_growth, pd.DataFrame(records)])

# compute specific points
pts = {35: [35, 65], 65: [65]}  # [Pa] estimation points per growth modality
extra = []
for j, (mod_growth, df) in enumerate(dfs):
    extra.append([mod_growth, []])
    for ca in pts[mod_growth]:
        an, = np.interp([ca], df['ca'], df['an'])
        ci, = np.interp([ca], df['ca'], df['ci'])
        gs, = np.interp([ca], df['ca'], df['gs'])

        print(f"extra {mod_growth} [Pa CO2] {ca:.1f}, {ci:.1f}, {an:.1f}, {gs:.1f}")
        extra[-1][1].append(dict(ca=ca, an=an, ci=ci, gs=gs))

# %%
# plot result only correcting error in legend of x axis (Ci instead of ca)
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(12, 5), squeeze=False)

for j, (mod_growth, df) in enumerate(dfs):
    ax = axes[0, j]
    ax.set_title(f"Grown at {mod_growth} [Pa CO2]")
    ax.plot(df['ci'], df['an'], label="from params")
    lmeas = meas.loc[mod_growth]
    ax.plot(lmeas['ca'], lmeas['an'], label="article")

    ax.legend(loc='upper left')
    ax.set_xlabel("Intercellular CO2 (Ci) [Pa]")

ax = axes[0, 0]
ax.set_xlim(0, 80)
ax.set_ylim(0, 35)
ax.set_ylabel("Net photosynthesis [µmol CO2.m-2.s-1]")

# add specific points
for j, (mod_growth, pts) in enumerate(extra):
    ax = axes[0, j]
    for pt in pts:
        ax.plot([pt['ci']], [pt['an']], 'o', color='#aaaaaa')
        ax.plot([0, pt['ci'], pt['ca']], [pt['an'], pt['an'], 0], '--', color='#aaaaaa')

fig.tight_layout()

# %%
# plot keeping ca as x axis
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(12, 5), squeeze=False)

for j, (mod_growth, df) in enumerate(dfs):
    ax = axes[0, j]
    ax.set_title(f"Grown at {mod_growth} [Pa CO2]")
    ax.plot(df['ca'], df['an'], label="from params")

    ax.set_xlabel("CO2 partial pressure (ca) [Pa]")

ax = axes[0, 0]
ax.set_xlim(0, 80)
ax.set_ylim(0, 35)
ax.set_ylabel("Net photosynthesis [µmol CO2.m-2.s-1]")

# add specific points
for j, (mod_growth, pts) in enumerate(extra):
    ax = axes[0, j]
    for pt in pts:
        ax.plot([pt['ca']], [pt['an']], 'o', color='#aaaaaa')
        ax.plot([0, pt['ca'], pt['ca']], [pt['an'], pt['an'], 0], '--', color='#aaaaaa')

fig.tight_layout()
plt.show()
