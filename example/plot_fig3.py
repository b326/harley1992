"""
Figure 3
========

Plot formalisms using parameter values from fig3 (table1)
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from harley1992 import pth_clean, raw

# params
tab1 = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['name', 'ca'])['value']

# recompute curves
temps = np.linspace(10, 40, 100)
df = {}
names = ['vc_max', 'j_max', 'tpu']

for name in names:
    records = []

    for mod_growth in (35, 65):
        for t_leaf in temps:
            tk = t_leaf + 273.15
            val = raw.eq9(tk,
                          tab1.at[(f'{name}_c', mod_growth)],
                          tab1.at[(f'{name}_deltaha', mod_growth)],
                          tab1.at[(f'{name}_deltahd', mod_growth)],
                          tab1.at[(f'{name}_deltas', mod_growth)])
            records.append(dict(
                mod_growth=mod_growth,
                t_leaf=t_leaf,
                val=val,
            ))

    df[name] = pd.DataFrame(records)

# plot result
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(12, 8), squeeze=False)

for name, ax in zip(names, axes.flatten()):

    for mod_growth, sdf in df[name].groupby('mod_growth'):
        ax.plot(sdf['t_leaf'], sdf['val'], label=f"{mod_growth:.0f}")

    ax.legend(loc='upper left', title="Oxygen")
    ax.set_xlabel("Leaf temperature [°C]")

    ax.set_xlim(10, 40)
    ax.set_ylabel(f"{name} [µmol.m-2.s-1]")

axes[1, 1].set_visible(False)

fig.tight_layout()
plt.show()
