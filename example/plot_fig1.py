"""
Figure 1
========

Plot measures from figure 1 and model evaluation using raw formalisms
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from harley1992 import pth_clean, raw

# read meas
meas = pd.read_csv(pth_clean / "fig1.csv", sep=";", comment="#", index_col=['ca', 'o2'])

# params
t_leaf = 29  # [°C] from text top of p274
tk = t_leaf + 273.15
ppfd = 1500  # [µmol.m-2.s-1] from text top of p274

fig1_params = pd.read_csv(pth_clean / "fig1_params.csv", sep=";", comment="#", index_col=['mod_growth', 'mod_tpu'])
tab1 = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['name', 'ca'])['value']

# recompute curves
records = []

for mod_growth in (35, 65):
    # table1
    alpha = tab1.at[('alpha', mod_growth)]

    # for the following 3 params there seem to be no tau_25, kc_25, ko_25 :'(
    tau = raw.eq8(tk, tab1.at[('tau_c', mod_growth)], tab1.at[('tau_deltaha', mod_growth)])
    kc = raw.eq8(tk, tab1.at[('kc_c', mod_growth)], tab1.at[('kc_deltaha', mod_growth)])
    ko = raw.eq8(tk, tab1.at[('ko_c', mod_growth)], tab1.at[('ko_deltaha', mod_growth)])

    for o2 in (2e3, 20e3):  # [Pa]
        for mod_tpu in ('lim', 'inf'):
            rd = fig1_params.at[(mod_growth, mod_tpu), 'rd']
            j_max = fig1_params.at[(mod_growth, mod_tpu), 'j_max']
            vc_max = fig1_params.at[(mod_growth, mod_tpu), 'vc_max']
            tpu = fig1_params.at[(mod_growth, mod_tpu), 'tpu']
            if pd.isna(tpu):
                tpu = 1e6  # almost inf

            j = raw.eq7(alpha, ppfd, j_max)

            # computes an
            for ci in np.linspace(1e-6, 80, 100):  # [Pa]
                wc = raw.eq4(vc_max, ci, o2 * 1e-3, kc, ko)
                wj = raw.eq5(j, ci, o2, tau)
                vc = 0  # heuristic
                wp = raw.eq6(tpu, vc, ci, o2, tau)

                vc = raw.eq2(wc, wj, wp)
                an = raw.eq1(vc, ci, o2, tau, rd)

                records.append(dict(
                    mod_growth=mod_growth,
                    mod_tpu=mod_tpu,
                    o2=o2,
                    ci=ci,
                    an=an,
                ))

df = pd.DataFrame(records).sort_values(by=['mod_growth', 'o2', 'mod_tpu', 'ci'])

# plot result
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(10, 5), squeeze=False)

for j, (mod_growth, df_growth) in enumerate(df.groupby('mod_growth')):
    ax = axes[0, j]
    ax.set_title(f"Grown at {mod_growth:d} [Pa CO2]")
    for o2, df_o2 in df_growth.groupby('o2'):
        lmeas = meas.loc[(mod_growth, int(o2 * 1e-3))]
        crv, = ax.plot(lmeas['ci'], lmeas['an'], 'o')

        sdf = df_o2[df_o2['mod_tpu'] == 'lim']
        ax.plot(sdf['ci'], sdf['an'], color=crv.get_color(), label=f"{o2 * 1e-3:.0f}")
        sdf = df_o2[df_o2['mod_tpu'] == 'inf']
        ax.plot(sdf['ci'], sdf['an'], '--', color=crv.get_color())

    ax.legend(loc='upper left', title="Oxygen")
    ax.set_xlabel("Intercellular CO2 [Pa]")

axes[0, 0].set_ylim(0, 40)
axes[0, 0].set_ylabel("Net photosynthesis [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
