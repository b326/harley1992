"""
Temperature dependency
======================

Plot evolution of all parameters that depend on leaf temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from harley1992 import pth_clean, raw

# read data
fig1_params = pd.read_csv(pth_clean / "fig1_params.csv", sep=";", comment="#", index_col=['mod_growth', 'mod_tpu'])

tab1 = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['ca'])
pval = tab1.loc[35].set_index('name')['value'].to_dict()

ts = np.linspace(0, 50, 100)

# plot data
fig, axes = plt.subplots(2, 4, sharex='all', figsize=(10, 7), squeeze=False)

for j, (name, unit) in enumerate([('kc', "Pa"), ('ko', "kPa"), ('tau', "-")]):
    ax = axes[0, j]
    ax.set_title(name)
    ax.plot(ts, [raw.eq8(t_leaf + 273.15,
                         pval[f'{name}_c'],
                         pval[f'{name}_deltaha']) for t_leaf in ts])
    ax.set_ylabel(f"[{unit}]")

# rd is specific
oxygen = 20e3  # [Pa]
name, unit = 'gamma_star', "Pa"
ax = axes[0, -1]
ax.set_title(name)
ax.plot(ts, [0.5 * oxygen / raw.eq8(t_leaf + 273.15,
                                    pval['tau_c'],
                                    pval['tau_deltaha']) for t_leaf in ts])
ax.set_ylabel(f"[{unit}]")

for j, name in enumerate(['vc_max', 'j_max', 'tpu']):
    ax = axes[1, j]
    ax.set_title(name)
    crv, = ax.plot([29]*4, fig1_params[name], 'o')
    ax.plot(ts, [raw.eq9(t_leaf + 273.15,
                         pval[f'{name}_c'],
                         pval[f'{name}_deltaha'],
                         pval[f'{name}_deltahd'],
                         pval[f'{name}_deltas']) for t_leaf in ts],
            color=crv.get_color())

    ax.set_ylabel("[µmol.m-2.s-1")

axes[-1, -1].set_visible(False)
for ax in axes[-1, :]:
    ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
